import databaseWork as dbWork
import variableWork as varWork 
import similarity as simWork
import random

def respond(condition, user_message):
    global selected_pic
    global selected_focus
    global pic_dic
    global focus_required
    
    response = {}
    response["text"] = "responding"  
    
    if condition=="pic_selected":
        selected_pic = user_message["selected_pic"]
        hyperlink_format = str(varWork.url_dic[selected_pic])
        outText = hyperlink_format.format(link='http://foo/bar', text='klicken Sie hier')
        att = [ varWork.pic_att_dic[selected_pic]
        ]
        response["text"] = " Das ausgewählte Bild ist: " + str(varWork.pic_dic[selected_pic]) + ". "
        response["attachment"] = att
    elif condition=="pic_not_selected":
        result_candidates = varWork.response_dict["picNotset"]
        response["text"] = random.choice(result_candidates)
    elif condition =="focus_selected":
        params = {}
        params["pictureID"] = user_message["selected_pic"]
        params["categoryValue"]  = user_message["selected_intent"]
        params["focusValue"]     = user_message["selected_focus"]
        response["text"] = dbWork.getSentences(params)
    elif condition =="focus_required":
        selected_pic    = user_message["selected_pic"]
        selected_intent = user_message["selected_intent"]
        noCats          = user_message["noCats"]
        
        lstCat = dbWork.getFocus(selected_pic,selected_intent)        
        response["text"] = "In diesem Zusammenhang gibt es " + str(noCats) + " Fokustexte ("+ ', '.join(lstCat) + "). Bitte wählen Sie einen Fokustext aus."
        response["attachment"] = lstCat

    elif condition == "notCat_found":
        selected_pic    = user_message["selected_pic"]
        
        params = {}
        params["pictureID"] = selected_pic
        lstCats       = dbWork.getCategories(params)
        print(lstCats)
        lstCats.remove('Other')
        lstParaphrase = [varWork.category_rephrase[cat] for cat in lstCats]
        
        response["text"] = " Ich habe leider keine Antwort auf diese Frage. Für dieses Bild habe ich Informationen zu folgenden Themen: (" + "/".join(lstParaphrase) +")"
    elif condition == "notFocus_found":
        response["text"] = "Wir brauchen einen Fokus Auswahl"
    
    elif condition == "conf_low":
        selected_pic = user_message["selected_pic"]
        sent         = user_message["sent"]
        output = simWork.sent2Output(sent, selected_pic)
        response["text"]     = output["text"]
        response["sessText"] = output["sessText"]
        
    elif condition == "session_running":
        username    = user_message["username"]
        response["text"] = "eine Benutzersitzung läuft. " + username + " müssen es versuchen, nachdem die aktuelle Sitzung geschlossen wurde."
    elif condition == "session_not_added":
        response["text"] = "eine Sitzung ist bereits in diesem Kanal hinzugefügt worden"
    elif condition == "session_added":
        sessionID    = user_message["sessionID"]
        response["text"] = "Für Sie wurde eine Mappe angelegt. ID : " + sessionID
    elif condition == "session_already_added":
        response["text"] = "Sie haben bereits eine Sitzung gestartet. Um die Sitzung zu beenden, gehen Sie durch sessionOff."
    elif condition == "session_deleted":
        sessionID    = user_message["sessionID"]
        response["text"] = "Ihre Sitzung " + sessionID + " ist beendet"
    elif condition == "session_del_otheruser":
        response["text"] = "Sie können eine Sitzung, die von einem anderen Benutzer erstellt wurde, nicht deaktivieren"
    elif condition == "session_del_noSession":
        response["text"] = "Es gibt keine Sitzung zu beenden. Bitte starten Sie eine Sitzung zuerst, obwohl sessionOn"
    elif condition =="general":
        params = {}
        params["pictureID"] = user_message["pictureID"]
        params["categoryValue"]  = user_message["category"]

        response["text"] = dbWork.getSentences(params)
         
    return response
