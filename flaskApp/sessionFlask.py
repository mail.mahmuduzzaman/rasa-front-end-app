import datetime
import time

def checkSession(transport_para):
    activeSession_dic          = transport_para["activeSession_dic"]
    botID                      = transport_para["botID"]
    mode                       = transport_para["mode"]
    
    if (botID not in activeSession_dic.keys()):
        return False
    else:
        currentInteraction  = activeSession_dic[botID]["currentInteraction"]
        if(currentInteraction!=mode):
            return False
        return True

def createSession(transport_para):
    print(transport_para)
    
    activeSession_dic          = transport_para["activeSession_dic"]
    botID                      = transport_para["botID"]
    mode                       = transport_para["mode"]
            
    currentInteraction = mode
    
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%d_%H%M%S')
    sessionID = botID +"_"+st +"_"+currentInteraction
    fileID    = sessionID + ".txt"
    
    activeSession_dic[botID]          = {"botID": botID, 
                                              "sessionID": sessionID, 
                                              "fileID": fileID,
                                              "currentInteraction":currentInteraction, 
                                              "picSelected" : False, 
                                              "currentPicture": None, 
                                              "focusRequired": False, 
                                              "currentFocus": None, 
                                              "currentIntent":None, 
                                              "intentConf": 0.0}
    
    response = {}
    response["text"]                        = "added"
    response["activeSession_dic"]           = activeSession_dic
    response["sessionID"]                   = sessionID
    response["currentInteraction"]          = currentInteraction
    return response

def deleteSession(transport_para):
    activeSession_dic          = transport_para["activeSession_dic"]
    botID                      = transport_para["botID"]
    if botID in activeSession_dic.keys():
        activeSession_dic.pop(botID, None)
        
    
    response = {}
    response["text"]                        = "deleted"
    response["activeSession_dic"]           = activeSession_dic
    return response