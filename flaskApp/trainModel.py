from rasa_nlu.training_data import load_data
from rasa_nlu.config import RasaNLUModelConfig
from rasa_nlu.model import Trainer
from rasa_nlu import config
from rasa_nlu.components import ComponentBuilder

#builder to use
builder = ComponentBuilder(use_cache=True)

# loading museum data
training_data = load_data("data.json")

#configuration
config = RasaNLUModelConfig(configuration_values={'language': 'de_core_news_sm',
                                                  'pipeline': 'spacy_sklearn'}) 
# trainer to educate our pipeline
trainer = Trainer(config, builder) 

# train the model!
interpreter = trainer.train(training_data)

# store it for future use
model_directory = trainer.persist("./models", fixed_model_name="museum")

# Ref : https://legacy-docs.rasa.com/docs/nlu/0.15.1/python/