import sqlite3

def getListCatFocus(picID):
    database = "dbBot.db"
    query    = "select categoryValue,focusValue from tblPicture where pictureID = '" + picID + "' group by categoryValue,focusValue "
    #print(query)
    conn = sqlite3.connect(database)
    # Create a cursor
    c = conn.cursor()
    # Execute the query
    c.execute(query)
    # Return the results
    outcome    =c.fetchall()
    print(outcome)
    #categories = [r[0] for r in outcome]
    return outcome
    
def getCountCategories(picID, intent):
    database = "dbBot.db"
    query    = "select count(DISTINCT focusValue) from tblPicture where pictureID = '" + picID + "' and categoryValue = '" + intent +"'" 
    print(query)
    conn = sqlite3.connect(database)
    # Create a cursor
    c = conn.cursor()
    # Execute the query
    c.execute(query)
    # Return the results
    outcome =c.fetchall()
    return outcome[0][0]
def getFocus(picID, intent):
    database = "dbBot.db"
    query    = "select DISTINCT focusValue from tblPicture where pictureID = '" + picID + "' and categoryValue = '" + intent +"'" 
    #print(query)
    conn = sqlite3.connect(database)
    # Create a cursor
    c = conn.cursor()
    # Execute the query
    c.execute(query)
    # Return the results
    outcome    =c.fetchall()
    categories = [r[0] for r in outcome]
    return categories
def getCategories(params):
    database = "dbBot.db"
    picID = params["pictureID"]
    query    = "select DISTINCT categoryValue from tblPicture where pictureID = '" + picID + "'" 
    #print(query)
    conn = sqlite3.connect(database)
    # Create a cursor
    c = conn.cursor()
    # Execute the query
    c.execute(query)
    # Return the results
    outcome    =c.fetchall()
    categories = [r[0] for r in outcome]
    return categories
def getSentences(params):
    database = "dbBot.db"
    #print(params)
    query    = "select sentenceValue from tblPicture" 
    if len(params) > 0:
        filterList=[]
        for key, val in params.items(): 
            filterList.append(key+" = '" +val +"'") 
        print(filterList)
        query += " WHERE " + " and ".join(filterList)

    #print(query)
    conn = sqlite3.connect(database)
    # Create a cursor
    c = conn.cursor()
    # Execute the query
    c.execute(str(query))
    # Return the results
    outcome    =c.fetchall()
    #print(outcome)
    sentences = [r[0] for r in outcome]
    #print(' '.join(sentences))
    return ' '.join(sentences)

