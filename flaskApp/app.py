import datetime
import time
import os
import openpyxl
import random
import flaskApp.sessionFlask as sessWork
import flaskApp.responseWork as resWork
import flaskApp.databaseWork as dbWork
import variableWork as varWork
import similarity as simWork


#Rasa NLU codes
##--from rasa_nlu.converters import load_data
from rasa_nlu.components import ComponentBuilder
builder = ComponentBuilder(use_cache=True)

from rasa_nlu.model import Metadata, Interpreter
interpreter_museum = Interpreter.load('./models/default/museum',builder)
#interpreter_test   = Interpreter.load('./models/default/museum',builder)

#Active Session dictionary
activeSession_dic          = {}
activeSession_dic_bd       = {}
activeSession_dic_urdu     = {}

params = {}
pic_selected    = False
focus_required  = False
selected_pic    = ""
selected_focus  = ""
selected_intent = ""
botListen       = False
currentUser     = ""

#temporary outText
outText         = ""
imageUrl        = ""
responseOpt     = []

from flask import Flask,request, make_response, Response
import json
app = Flask(__name__)

@app.route('/', methods=['GET'])
def base1():
    option = request.args.get('option') 
    if option =="home":
        msgText = request.args.get('text') 
        botID   = request.args.get('botID')  
        #return home_method(msgText, botID)
        return home()
    elif option == "mode":
        return mode()
        
@app.route('/home', methods=['GET'])
def home():
    global outText
    global imageUrl
    global responseOpt
    global activeSession_dic
    
    #input from the request
    msgText = request.args.get('text') 
    botID   = request.args.get('botID')  
    
    if checkSession(botID, activeSession_dic):
        #handle input text
        handle_command(msgText,botID)    
    
        retText = outText
        data= {"text" : retText,
               "imageUrl": imageUrl,
               "respOpt" : responseOpt}
        json_str = json.dumps(data)
    else:
        retText = "ModeNotSet"
        data= {"text" : retText,
               "imageUrl": imageUrl,
               "respOpt" : responseOpt}
        json_str = json.dumps(data)
    return json_str

@app.route('/mode', methods=['GET'])
def mode():
    mode    = request.args.get('mode')
    botID   = request.args.get('botID')

    #Basic works
    sessionValidation(botID,mode)
    retText = mode
    data= {"text" : retText}
    json_str = json.dumps(data)
    return json_str



def checkSession(botID,sessionDictionary):

    if (botID not in sessionDictionary.keys()):
        return False
    else:
        return True
    
#Make session entry for bangla Dictionary
def set_session_Lang(botID,dictionary):
    
    ts        = time.time()
    st        = datetime.datetime.fromtimestamp(ts).strftime('%d_%H%M%S')
    sessionID = botID +"_"+st
    fileID    = sessionID + ".txt"
    
    dictionary[botID]          = {"botID": botID, 
                                          "sessionID": sessionID, 
                                          "fileID": fileID}

    
#Checking the session
def sessionValidation(botID, mode):
    
    global activeSession_dic
    global outText
    #Session Check
    transport_para = {}
    transport_para["activeSession_dic"]          = activeSession_dic    
    transport_para["botID"]                      = botID    
    transport_para["mode"]                       = mode
    
    response = sessWork.checkSession(transport_para)
    if response==False:
        #delete the session
        transport_para = {}
        transport_para["activeSession_dic"]          = activeSession_dic
        transport_para["botID"]                      = botID     
        response = sessWork.deleteSession(transport_para)
        
        activeSession_dic   = response["activeSession_dic"] 
        #insert into the active dic
        transport_para = {}
        transport_para["activeSession_dic"]          = activeSession_dic
        transport_para["botID"]                      = botID
        transport_para["mode"]                       = mode
        response = sessWork.createSession(transport_para)
        
        activeSession_dic   = response["activeSession_dic"] 
        currentInteraction  = response["currentInteraction"] 

        pic_selected        = False
        outText             = "sessionAdded "+ currentInteraction
    else:
        outText             = "sessionAlreadyThere"
        
        
def handle_command(input_text,botID):
    
    """
    Recieves commands directed for the bot, if they are valid perform action 
    else resends clarification
    """
    response_generated = ""
    global pic_selected
    global selected_pic
    global selected_focus
    global focus_required
    global focus_list
    global currentUser
    global activeSession_dic
    
    modeCurrentInteraction = ""
    
    EXAMPLE_COMMAND = 'do'
    
    #Setting the default message sending option a dictionary
    response_generated = {}
    response_generated["msgOption"] = "normal"
    
            
    #if checkNumeric(command):
    if input_text.isdigit():
        #print("here")
        if checkPicSelected(botID)==False:
            if int(input_text)>0 and int(input_text)<31:
                pic_selected, selected_pic = setPicture(input_text,botID)
                #pic_selected = True
                #selected_pic = varWork.pic_list[int(command)-1]
                transport_para = {}
                transport_para["selected_pic"] = getSelectedPicture(botID)
                respOutput = resWork.respond("pic_selected",transport_para)
                #generation of response
                response_generated["msgOption"]  = "picture"
                response_generated["text"]       = random.choice(varWork.response_dict["greet"]) + respOutput["text"]
                response_generated["attachment"] = respOutput["attachment"]
                #writing in log file
                msg = "Selected picture is : " + input_text
                writeInFile(msg, botID)
                
        else:
            #Changing the existing picture with other
            if int(input_text)>0 and int(input_text)<31:
                pic_selected, selected_pic = setPicture(input_text,botID)
                #pic_selected = True
                #selected_pic = varWork.pic_list[int(input_text)-1]
                transport_para = {}
                transport_para["selected_pic"] = getSelectedPicture(botID)
                respOutput = resWork.respond("pic_selected",transport_para)
                #generation of response
                response_generated["msgOption"] = "picture"
                response_generated["text"] = respOutput["text"]
                response_generated["attachment"] = respOutput["attachment"]
                #writing in log file
                msg = "Changed picture : " + input_text
                writeInFile(msg, botID)
        if int(input_text)<=0 or int(input_text)>=31:
            respOutput = resWork.respond("pic_not_selected","")
            response_generated["text"] = respOutput["text"]
    else:
        if checkPicSelected(botID)==False:
        #if pic_selected==False:
            #Could noint(input_text)>0 and int(input_text)<31t set the picture with the input message
            respOutput = resWork.respond("pic_not_selected","")
            response_generated["text"] = respOutput["text"]
        elif checkCurrentMode(botID) == "normal":
            print ("----------------")
            #This version is only for normal mode of interaction
            if checkFocusRequired(botID)==True:
            #if focus_required==True:
                if input_text.lower() in varWork.focus_list:
                    focus_required, selected_focus = setFocus(input_text,botID)
                    transport_para = {}
                    transport_para["selected_pic"]    = getSelectedPicture(botID)
                    transport_para["selected_intent"] = getSelectedIntent(botID)
                    transport_para["selected_focus"]  = getSelectedFocus(botID)
                    #print(transport_para)
                    respOutput = resWork.respond("focus_selected",transport_para)
                    response_generated["text"] = respOutput["text"]
                    #writing in log file
                    msg = "focus : " + getSelectedFocus(botID) + " category : " + getSelectedIntent(botID)
                    writeInFile(msg, botID)
                    #writing in log file
                    msg = "General response text : " + respOutput["text"]
                    writeInFile(msg, botID)

                else:
                    #writing in log file
                    msg = "focus not correct "
                    writeInFile(msg, botID)

                    respOutput = resWork.respond("notFocus_found","")
                    response_generated["text"] = respOutput["text"]
            else:
                msg = "\nQuestion asked : " + input_text
                writeInFile(msg, botID)

                #parsing the text and give response
                response_generated = general_response(input_text,botID)
        elif checkCurrentMode(botID) == "random":
            print ("----random mode---")
            response_generated["msgOption"] = "random"
            
            msg = "\nQuestion asked : " + input_text
            writeInFile(msg, botID)
            # we will deliver random contents
            response_generated["text"] = randomResponse(botID)
            msg = "Random response text : " + response_generated["text"]
            writeInFile(msg, botID)
          
    print("response id generated : " ,response_generated)
    
    if len(response_generated) is not 0:
        sendMessage(response_generated)
    else:
        print ('Invalid input_text: Not Understood')
        slack_api.rtm_send_message(channel, 'Invalid input_text: Not Understood')
        
def checkPicSelected(botID):
    return activeSession_dic[botID]["picSelected"]

def checkCurrentMode(botID):
    print(activeSession_dic)
    return activeSession_dic[botID]["currentInteraction"]

def sendMessage(response_generated):
    global outText
    global imageUrl
    global responseOpt
    
    if response_generated["msgOption"] =="normal":
        print("normal--")
        outText  = response_generated["text"]
        imageUrl = ""
        responseOpt = []
    elif response_generated["msgOption"] =="random":
        print("random--")
        outText  = response_generated["text"]
        imageUrl = ""
        responseOpt = []
    elif response_generated["msgOption"] == "button":
        outText     = response_generated["text"]
        imageUrl    = ""
        responseOpt = response_generated["attachment"]
    else:       
        outText  = response_generated["text"]
        imageUrl = response_generated["attachment"][0]['image_url']
        responseOpt = []
        print("hi--", imageUrl)


def setPicture(command,botID):
    pic_selected = True
    selected_pic = varWork.pic_list[int(command)-1]

    activeSession_dic[botID]["picSelected"] = pic_selected
    activeSession_dic[botID]["currentPicture"] = selected_pic
    return pic_selected, selected_pic

def getSelectedPicture(botID):
    return activeSession_dic[botID]["currentPicture"] 
    
def checkFocusRequired(botID):
    global focus_required
    return activeSession_dic[botID]["focusRequired"]
    
def general_response(user_message,botID):
    global focus_required
    
    found_intent = intent_classifier(user_message.lower())
    intentValue = found_intent["intent"]["name"]
    setIntent(user_message,botID,intentValue) # setting intent Value
    
    intent_conf = found_intent["intent"]["confidence"]
    
    print("intent : " + getSelectedIntent(botID))
    msg = "Intent found : " + getSelectedIntent(botID) + " confidence : " + str(intent_conf)
    writeInFile(msg, botID)
    
    outcome = {}
    outcome["msgOption"] ="normal"
    if intent_conf<=0.20:
        #check the similary exist
        if simWork.checkSimilarity(user_message.lower(), getSelectedPicture(botID)) ==True:
            transport_para = {}
            transport_para["selected_pic"]    = getSelectedPicture(botID)
            transport_para["sent"]    = user_message.lower()
            respOutput      = resWork.respond("conf_low",transport_para)
            outcome["text"] = respOutput["text"]
            #sess write
            msg = "From similary : " + respOutput["sessText"]
            writeInFile(msg, botID)
            msg = "From similary text : " + respOutput["text"]
            writeInFile(msg, botID)
            
        else:
            transport_para = {}
            transport_para["selected_pic"]    = getSelectedPicture(botID)
            respOutput       = resWork.respond("notCat_found",transport_para)
            outcome["text"]  = respOutput["text"]
            #sess write
            msg = "No similary found from similarity module"
            writeInFile(msg, botID)
        return outcome
    

    noCats = dbWork.getCountCategories(getSelectedPicture(botID),getSelectedIntent(botID))
    if noCats>1:
        focus_required=True
        setFocusRequired(botID,focus_required)
        
        transport_para = {}
        transport_para["selected_pic"]    = getSelectedPicture(botID)
        transport_para["selected_intent"] = getSelectedIntent(botID)
        transport_para["noCats"]          = noCats
        outcome["msgOption"] = "button"
        respOutput      =  resWork.respond("focus_required",transport_para)
        outcome["text"] =  respOutput["text"]
        outcome["attachment"] = respOutput["attachment"]
        #sess write
        msg = "multi focus found for this category"
        writeInFile(msg, botID)
        
    elif noCats==0:
        #find next intent
        tran_para = {}
        tran_para["found_intent"] = found_intent
        nextIntent  =  getNextMatchingCategory(user_message,tran_para)
        print("----------------------",nextIntent)
        #sess write
        msg = "No category found. next matching intent is " + nextIntent
        writeInFile(msg, botID)
        
        outcome["text"] = ""
        
        if nextIntent!="none":
            params = {}
            params["pictureID"] = getSelectedPicture(botID)
            params["category"]  = nextIntent

            respOutput      =  resWork.respond("general", params)
            outcome["text"] =  respOutput["text"] +"\n\n"
            #sess write
            msg = "Next intent text : " + outcome["text"]
            writeInFile(msg, botID)
        elif simWork.checkSimilarity(user_message.lower(), selected_pic) ==True:
            
            output               = simWork.sent2Output(user_message.lower(), getSelectedPicture(botID))
            outcome["text"]      = output["text"] +"\n\n"
            sessText             = output["sessText"]
            #sess write
            msg = "From similary : " + sessText
            writeInFile(msg, botID)
            msg = "From similary text : " + output["text"]
            writeInFile(msg, botID)
        #get response
        transport_para = {}
        transport_para["selected_pic"]    = getSelectedPicture(botID)
        respOutput       = resWork.respond("notCat_found",transport_para)
        print(respOutput)
        outcome["text"]  += respOutput["text"]
    else:
        params = {}
        params["pictureID"] = getSelectedPicture(botID)
        params["category"]  = getSelectedIntent(botID)
        
        msg = "Gave general response for " + selected_intent
        writeInFile(msg, botID)
        
        respOutput      =  resWork.respond("general", params)
        outcome["text"] =  respOutput["text"]
        
        msg = "General intent text : " + respOutput["text"]
        writeInFile(msg, botID)
    return outcome

def intent_classifier(user_message):
    best_intent = interpreter_museum.parse(user_message)
    print(best_intent)
    # print(best_intent["intent"]["confidence"])
    return best_intent

def setIntent(command,botID,intentValue):
        activeSession_dic[botID]["currentIntent"]    = intentValue

def getSelectedIntent(botID):
        return activeSession_dic[botID]["currentIntent"] 
    
def getNextMatchingCategory(user_message,tran_para):
    found_intent = tran_para["found_intent"]
    next_intent = found_intent["intent_ranking"][1]["name"]
    #get a list of options available
    params = {}
    params["pictureID"] = selected_pic
    lstCats       = dbWork.getCategories(params)
    lstCats.remove('Other')
    if next_intent in lstCats:
        return next_intent
    else:
        return "none"
    
def writeInFile(msg, botID):
    global activeSession_dic
    #print(msg,channel,user)
    if botID in activeSession_dic.keys():
        fileName = "sessionFiles/" + activeSession_dic[botID]["fileID"]
        #open file
        f=open( fileName, "a+")
        #write
        f.write(msg + "\n")
        #close file
        f.close() 

def writeInFileLang(msg, botID,dictionary, fileLocation):
    print(msg,dictionary, botID)
    if botID in dictionary.keys():
        fileName = fileLocation+ "/" + dictionary[botID]["fileID"]
        
        print(fileName)
        #open file
        f=open( fileName, "a+")
        #write
        f.write(msg + "\n")
        #close file
        f.close() 

def setFocusRequired(botID,focusReq):
    activeSession_dic[botID]["focusRequired"]    = focusReq
    
def setFocus(command,botID):
    focus_required = False
    selected_focus = command.lower()

    activeSession_dic[botID]["focusRequired"]    = focus_required
    activeSession_dic[botID]["currentFocus"]     = selected_focus
    return focus_required, selected_focus

def getSelectedFocus(botID):
    return activeSession_dic[botID]["currentFocus"] 

def randomResponse(botID):
    #get a list of category and focus of current picture
    lstCatFocus = dbWork.getListCatFocus(getSelectedPicture(botID))
    
    randomValue = random.randrange(0,len(lstCatFocus))
    sel_intent, sel_focus = lstCatFocus[randomValue]
    
    transport_para = {}
    transport_para["selected_pic"]    = getSelectedPicture(botID)
    transport_para["selected_intent"] = sel_intent
    transport_para["selected_focus"]  = sel_focus
    
    msg = "Gave random response for Intent : " + sel_intent + " Focus : " + sel_focus
    writeInFile(msg, botID)
    #print(transport_para)
    respOutput = resWork.respond("focus_selected",transport_para)
    outcome = respOutput["text"]
    return outcome
        
#if __name__ == "__main__":
#    app.run()

#Create a Session